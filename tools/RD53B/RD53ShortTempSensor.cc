#include "RD53ShortTempSensor.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include "../DQMUtils/RD53ShortTempSensorHistograms.h"


namespace RD53BTools {


template <class Flavor>
ChipDataMap<typename RD53ShortTempSensor<Flavor>::ChipResults> RD53ShortTempSensor<Flavor>::run() {
	constexpr float T0C = 273.15;         // [Kelvin]
	constexpr float kb  = 1.38064852e-23; // [J/K]
	constexpr float e   = 1.6021766208e-19;
	constexpr float R   = 15; // By circuit design
	
	ChipDataMap<ChipResults> results;
	auto& chipInterface = Base::chipInterface();

	Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");

	dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);
	
	
	uint16_t sensorConfigData;

	Base::for_each_chip([&] (Chip* chip) {
		auto& valueLow = results[chip].valueLow;
		auto& valueHigh = results[chip].valueHigh;
		auto& temperature = results[chip].temperature;
	
		for(int sensor=0;sensor<4;sensor++){				
			chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose VMUX entry
			chipInterface.WriteReg(chip, "VMonitor", sensor_VMUX[sensor]);
			
			valueHigh = 0;
			// Get high bias voltage
			for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){ //Unsure if this actually works
				sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 0, true, sensorDEM, 0);
				chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData); //Apply high bias
				valueHigh += dKeithley2410.getVoltage();
			}
			valueHigh = valueHigh/3;
			
			valueLow = 0;
			// Get low bias voltage
			for(int sensorDEM=0;sensorDEM<16;sensorDEM+=7){
				sensorConfigData = bits::pack<1, 4, 1, 1, 4, 1>(true, sensorDEM, 1, true, sensorDEM, 1);
				chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData); //Apply low bias
				valueLow += dKeithley2410.getVoltage();
			}
			valueLow = valueLow/3;
			
			temperature[sensor] = (e/(kb*log(R)))*(valueLow-valueHigh)/idealityFactor[sensor]-T0C;
		}
		//Get NTC temperature
		//temperature[4]=chipInterface.ReadHybridTemperature(chip); //Does not currently work, to be fixed
		chipInterface.WriteReg(chip, "VMonitor", 2);
		temperature[4]=dKeithley2410.getVoltage(); 
	});
	return results;
}

template <class Flavor>
void RD53ShortTempSensor<Flavor>::draw(const ChipDataMap<ChipResults>& results) const {
	for (const auto& item : results) {
		ShortTempSensorHistograms* histos = new ShortTempSensorHistograms;
		histos->fillSTS(
			item.second.temperature
		);
	}
}

template <class Flavor>
const double RD53ShortTempSensor<Flavor>::idealityFactor[];

template <class Flavor>
const int RD53ShortTempSensor<Flavor>::sensor_VMUX[];

template class RD53ShortTempSensor<RD53BFlavor::ATLAS>;
template class RD53ShortTempSensor<RD53BFlavor::CMS>;

}


