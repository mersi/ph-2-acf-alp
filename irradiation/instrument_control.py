import socket
import time

class PowerSupplyController:
    def __init__(self, powerSupplyId, outputs):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect(("127.0.0.1", 7000))
        self.powerSupplyId = powerSupplyId

    def power_on(self):
        self.s.sendall(f"TurnOn,PowerSupplyId:{self.powerSupplyId},ChannelId:0".encode())
        self.s.sendall(f"TurnOn,PowerSupplyId:{self.powerSupplyId},ChannelId:1".encode())

    def power_off(self):
        self.s.sendall(f"TurnOff,PowerSupplyId:{self.powerSupplyId},ChannelId:0".encode())
        self.s.sendall(f"TurnOff,PowerSupplyId:{self.powerSupplyId},ChannelId:1".encode())

    def set_voltage(self, channel, value):
        self.s.sendall(f"SetVoltage,PowerSupplyId:{self.powerSupplyId},ChannelId:{channel},Voltage:{value}".encode())

    def read_voltage(self, channel):
        self.s.sendall(f"GetVoltage,PowerSupplyId:{self.powerSupplyId},ChannelId:{channel}".encode())

    def set_current(self, channel, value):
        self.s.sendall(f"SetCurrent,PowerSupplyId:{self.powerSupplyId},ChannelId:{channel},Current:{value}".encode())

    def read_current(self, channel, value):
        self.s.sendall(f"GetCurrent,PowerSupplyId:{self.powerSupplyId},ChannelId:{channel}".encode())

    def power_cycle(self):
        power_off()
        time.sleep(.5)
        power_on()