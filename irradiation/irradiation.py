
[
    {
        "type": "Ph2_ACF",
        "interval": 1,
        "configFile": "CROC1.xml"
        "tools": ["AnalogScan", "DigitalScan"]
    },
    {
        "type": "IVConfigured",
        "interval" : 2,
        "configFile": "CROC2.xml"
            
    },
    {
        "type": "IVDefault",
        "interval" : 2,
        "startingCurrent" : 1,
        "finalCurrent" : 2,
        "currentStep" : 1 
    }
]

]